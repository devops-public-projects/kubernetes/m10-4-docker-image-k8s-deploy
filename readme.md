
# Deploy Docker Image to a K8s Cluster
Deploy our web application in K8s cluster from private Docker registry.

## Technologies Used
- Kubernetes
- Helm
- AWS ECR
- Docker

## Project Description
- Create Secret for credentials for the private Docker registry
- Configure the Docker registry secret in application
- Deployment component
- Deploy web application image from our private Docker registry in K8s cluster

## Prerequisites
- [Nodejs Project Code](https://gitlab.com/twn-devops-bootcamp/latest/10-kubernetes/js-app)
- [Project Starting Code](https://gitlab.com/twn-devops-bootcamp/latest/10-kubernetes/deploying-images-from-private-docker-repo/-/tree/starting-code)
- AWS Elastic Container Registry
	- Name: my-app
	- Visibility: Private
- Sample Nodejs project uploaded to ECR, 1 or 2 versions available
- Minikube installed and configured

## Guide Steps
### Configure Docker and ECR
- Get login password for AWS ECR and login with Docker (Obtained from the `ECR Push Commands` section)
	- `aws ecr get-login-password --region us-east-1 | docker login --username AWS --password-stdin ID.dkr.ecr.us-east-1.amazonaws.com`
- Print out the contents of the get-login-password
	- `aws ecr get-login-password`
- SSH into Minikube  (Required as Minikube cannot access credentials on your machine directly)
	- `minikube ssh`
- Perform AWS Login inside Minikube
	- `docker login --username AWS -p CONTENTS_OF_PREVIOUS_COMMAND ID.dkr.ecr.us-east-1.amazonaws.com`
- Verify Successful Login (A .docker directory and **config.json** are created)
	- `ls ~/.docker` 

### Create Secret Component
- Backup your existing config.json if you need to
	- `cp ~/.docker/config.json ~/.docker/config.json_bak`
- Copy the Minikube config.json to your computer
	- `minikube cp minikube:/home/docker/.docker/config.json /user/chris/.docker/config.json`
- Create Secret
 ```bash
kubectl create secret generic my-registry-key \
--from-file=.dockerconfigjson=~/.docker/config.json \
--type=kubernetes.io/dockerconfigjson
```
- Verify Secret Creation
	- `kubectl get secret`

### Create Deployment Component
- Open `my-app-deployment.yaml` and add the image name from your repository
	- `image: ID.dkr.ecr.us-east-1.amazonaws.com/my-app:1.0`
- Apply the YAML
	- `kubectl apply -f my-app-deployment.yaml`
- Check Pod Status
	- `kubectl get pod`
- Check Pod Events
	- `kubectl describe pod APP_POD`

![Successful Deploy](/images/m10-4-successful-deploy.png)

![Describe Pod](/images/m10-4-pod-information.png)